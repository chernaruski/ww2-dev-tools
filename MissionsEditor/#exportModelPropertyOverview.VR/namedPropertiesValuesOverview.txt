aicovers	"0"
armor	"1"	"10"	"100"	"1000"	"10000"	"1100"	"1250"	"150"	"1500"	"15000"	"1750"	"1900"	"20"	"200"	"2000"	"2100"	"2200"	"2500"	"2700"	"2800"	"3"	"300"	"3000"	"350"	"3500"	"400"	"4000"	"5"	"50"	"500"	"5000"	"600"	"6000"	"700"	"7000"	"750"	"80"	"800"	"8000"	"9"
author	"maa"
autocenter	"0"	"1"
buoyancy	"1"
canbeoccluded	"0"	"1"
canocclude	"0"	"1"	"no"
class	"building"	"bunker"	"bush"	"bushHard"	"bushSoft"	"church"	"clutter"	"fence"	"house"	"housesimulated"	"land_decal"	"man"	"road"	"ruins"	"streetlamp"	"thing"	"tower"	"treeHard"	"treeSoft"	"vehicle"	"wall"	"pond"
damage	"0"	"building"	"fence"	"no"	"tent"	"tree"	"wall"	"none"	"buiding"	"buildang"	"house"	"treehard"	"wreck"
dammage	"0"	"building"	"bunker"	"destructno"	"engine"	"house"	"no"	"none"	"tent"	"tree"	"wall"
destroysound	"treebroadleaf"	"treepalm"
destruct	"building"	"tent"
drawimportance	"0.2"
explosionshielding	"0.05"	"0.1"	"10"	"100"	"1000"	"20"	"3"
forcenotalpha	"1"
frequent	"1"
keyframe	"1"
loddensitycoef	"0.45"	"0.5"	"0.75"	"1.25"
lodneeded	"2"	"3"	"4"	"5"
lodnoshadow	"0"	"1"	"1."	"2"	"10"	"3"	"4"	"5"
map	"building"	"bunker"	"bush"	"busstop"	"chapel"	"church"	"cross"	"fence"	"fortress"	"fountain"	"fuelstation"	"hide"	"hospital"	"house"	"lighthouse"	"main road"	"no"	"none"	"power lines"	"powersolar"	"powerwave"	"powerwind"	"quay"	"railway"	"road"	"rock"	"ruin"	"shipwreck"	"small tree"	"smalltree"	"stack"	"tourism"	"track"	"transmitter"	"tree"	"view-tower"	"wall"	"watertower"
mass	"0"	"0.000000"	"0.200000"	"0.500000"	"1.000000"	"100.000000"	"100.000023"	"100000.000000"	"100001.132813"	"2007.330811"	"5.000000"	"7"	"92079.242188"
placement	"landcontact"	"slope"	"slopeLandContact"	"slopeX"	"slopez"	"vertical"
prefershadowvolume	"0"	"1"
rock	"1"
sbsource	"explicit"	"explicite"	"none"	"shadow"	"shadowVolume"	"visual"	"visualex"
shadow	"hybrid"	"no"
shadowbufferlod	"0"	"1000"	"1500"	"1010"
shadowlod	"0"	"10"	"100"
shadowoffset	"0.001"	"0.02"
shadowoffsetforced	"1.000"	"1.0000"	"1.000000"
shadowvolumelod	"0"	"10"
step	"0.000000"	"-0.000000"	"0.062069"	"-0.000005"	"-3.128914"	"0"	"0.004653"	"1.719450"	"-0.000331"	"-3.210750"	"-0.060549"	"-0.093582"	"-1.500000"	"-1.686230"	"-4.846974"	"0.009227"
style	"creature"	"machinery"	"plant"	"structure"
transparent	"1"
viewdensitycoef	"0.1"	"10"	"10000"
xcount	"6"
xsize	"0.6534"	"0.99"	"1.3167"	"1.5"
xstep	"0.000000"	"-0.000000"	"-0.000002"	"-0.000267"	"-0.025455"	"0"	"0.000001"	"0.000020"	"0.000144"	"-0.011045"	"0.008392"	"0.001785"	"0.112641"	"-0.000161"
ycount	"5"
ysize	"0.34848"	"0.528"	"0.70224"	"0.8"
